name := "kc-authentication-service"
organization := "com.jackeri"
maintainer := "captain@jackeri.com"

version := "0.1"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

scalaVersion := "2.13.1"

libraryDependencies ++= Seq(
  guice,
  evolutions,
  javaJdbc,
  "org.projectlombok" % "lombok" % "1.18.12" % "provided",
  "mysql" % "mysql-connector-java" % "8.0.19",
  "com.auth0" % "java-jwt" % "3.10.1",
  "org.mindrot" % "jbcrypt" % "0.4"
)

// Test configuration
libraryDependencies ++= Seq(
  "com.h2database" % "h2" % "1.4.200" % Test
)
javaOptions in Test += "-Dconfig.resource=test.conf"