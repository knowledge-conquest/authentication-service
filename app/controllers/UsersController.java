package controllers;


import com.fasterxml.jackson.databind.JsonNode;
import models.UserEntity;
import play.libs.Json;
import play.mvc.*;
import services.PasswordService;
import services.jwt.JWTRequired;

import javax.inject.Inject;

/**
 * UsersController handles the requests on the user entity.
 */
@BodyParser.Of(BodyParser.Json.class)
public class UsersController extends Controller {

    private PasswordService passwordService;

    @Inject
    public UsersController(PasswordService passwordService) {
        this.passwordService = passwordService;
    }

    public Result createUser(Http.Request request) {

        JsonNode json = request.body().asJson();
        UserEntity user = UserEntity.fromJson(json);

        // hash password
        user.setPassword(passwordService.hashPassword(user.getPassword()));

        user.insert();

        return ok(Json.toJson(user));
    }

    @JWTRequired
    public Result getUserById(Long id) {
        UserEntity user = UserEntity.find.byId(id);
        return user != null ? ok(user.toJson()) : notFound();
    }
}
