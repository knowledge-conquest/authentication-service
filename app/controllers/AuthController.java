package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import models.UserEntity;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.PasswordService;
import services.jwt.JWTManager;

import javax.inject.Inject;

public class AuthController extends Controller {

    private JWTManager jwtManager;
    private PasswordService passwordService;

    @Inject
    public AuthController(JWTManager jwtManager, PasswordService passwordService) {
        this.jwtManager = jwtManager;
        this.passwordService = passwordService;
    }

    public Result authenticate(Http.Request request) {

        JsonNode json = request.body().asJson();

        if (!json.has("username") || ! json.has("password")) {
            return badRequest();
        }

        String username = json.get("username").asText();
        String password = json.get("password").asText();

        UserEntity user = UserEntity.find.query().where().eq("username", username).findOne();

        // Check if the user exists
        if (user == null) {
            return notFound();
        }

        // Check if the passwords match
        if (!passwordService.checkPassword(password, user.getPassword())) {
            return unauthorized();
        }

        // return the token
        return ok(Json.toJson(new AuthResponse(user.getId())));
    }

    private class AuthResponse {

        @Getter @Setter
        private String token;

        @Getter @Setter
        private long userId;

        public AuthResponse(long userId) {
            this.userId = userId;
            this.token = jwtManager.createUserToken(userId);
        }
    }
}
