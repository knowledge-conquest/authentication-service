package services.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.typesafe.config.Config;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class JWTManager {

    private static final String SECRET_CONFIG_KEY = "jwt.secret";
    private static final String ISSUER_CONFIG_KEY = "jwt.issuer";
    private static final String USER_ID_KEY = "userId";

    private final String ISSUER;
    private Algorithm algorithm;
    private JWTVerifier verifier;

    @Inject
    public JWTManager(Config config) {

        String secret = config.getString(SECRET_CONFIG_KEY);
        ISSUER = config.getString(ISSUER_CONFIG_KEY);

        algorithm = Algorithm.HMAC256(secret);
        verifier = JWT.require(algorithm)
                .withIssuer(ISSUER)
                .build();
    }

    public String createUserToken(Long id) {
        return JWT.create().withIssuer(ISSUER).withClaim(USER_ID_KEY, id).sign(algorithm);
    }

    public VerifiedJWT verify(String token) {
        return new VerifiedJWT(verifier.verify(token));
    }
}
