package services.jwt;

import com.auth0.jwt.exceptions.JWTVerificationException;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import utils.Attrs;

import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

/**
 * This action checks the authorization header and extracts the bearer token.
 * Throws a JWTVerificationException if there is no token or if its verification failed.
 */
public class JWTRequiredAction extends Action<JWTRequired> {

    private static final String AUTH_HEADER_NAME = "Authorization";
    private static final String AUTH_HEADER_PREFIX = "Bearer ";

    private JWTManager jwtManager;

    @Inject
    public JWTRequiredAction(JWTManager jwtManager) {
        this.jwtManager = jwtManager;
    }

    @Override
    public CompletionStage<Result> call(Http.Request request) throws JWTVerificationException {

        Optional<String> bearer = request.getHeaders().get(AUTH_HEADER_NAME);

        if (!bearer.isPresent() || !bearer.get().startsWith(AUTH_HEADER_PREFIX)) {
            throw new NoJWTException();
        }

        String token = bearer.get().substring(AUTH_HEADER_PREFIX.length());
        request.addAttr(Attrs.VERIFIED_JWT, jwtManager.verify(token));

        return delegate.call(request);
    }
}
