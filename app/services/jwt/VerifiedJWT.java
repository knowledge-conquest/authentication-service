package services.jwt;

import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.Getter;

import java.util.Date;

public class VerifiedJWT {

    private DecodedJWT decodedJWT;

    @Getter
    private boolean expired;

    public VerifiedJWT(DecodedJWT decodedJWT) {
        this.decodedJWT = decodedJWT;
        this.expired = decodedJWT.getExpiresAt() != null && new Date().after(decodedJWT.getExpiresAt());
    }

    public Claim getClaim(String name) {
        return decodedJWT.getClaim(name);
    }
}
