package services.jwt;

import com.auth0.jwt.exceptions.JWTVerificationException;

public class NoJWTException extends JWTVerificationException {

    public NoJWTException() {
        super("No JWT found");
    }
}
