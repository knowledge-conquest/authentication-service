package utils;

import models.UserEntity;
import play.libs.typedmap.TypedKey;
import services.jwt.VerifiedJWT;

public class Attrs {
    public static final TypedKey<VerifiedJWT> VERIFIED_JWT = TypedKey.create("VerifiedJWT");
    public static final TypedKey<UserEntity> USER = TypedKey.create("User");
}
