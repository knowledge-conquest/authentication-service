package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.annotation.NotNull;
import lombok.Getter;
import lombok.Setter;
import play.libs.Json;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity @Table(name = "user") @JsonIgnoreProperties("_$dbName")
public class UserEntity extends Model {

    @Id @Getter
    private Long id;

    @NotNull @Getter @Setter
    private String username;

    @NotNull @Getter @Setter
    private String email;

    @Getter @Setter
    @NotNull @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    public UserEntity(String email, String username, String password) {
        this.email = email;
        this.username = username;
        this.password = password;
    }

    public JsonNode toJson() {
        return Json.toJson(this);
    }

    public static UserEntity fromJson(JsonNode json) {
        return Json.fromJson(json, UserEntity.class);
    }

    public static final Finder<Long, UserEntity> find = new Finder<>(UserEntity.class);
}
