# Authentication Service

Microservice containing user information such as credentials or e-mail addresses. Mainly used for user authentication.

## Environment variables

This project uses environement variables. In brakcet lies the default value.

- HTTP_SECRET (`changeme`): Contains the http secret required by play to run properly in deployment mode.
- HTTP_PORT (`8080`): The port on which the application should run (Might have no effect in development run).
- JWT_ISSUER (`kc-auth`): The issuer of the JWT tokens used for authentication.
- JWT_SECRET (`secret`): The secret for JWT tokens.
- DB_HOST (`localhost`): The host of the MySQL databse.
- DB_PORT (`3306`): The port of the MySQL database.
- DB_NAME (`kc-auth`): The name of the databse to use.
- DB\_URL (`jdbc:mysql://DB_HOST:DB_PORT/DB_NAME`): The complete url to the database (usually not modified).
- DB_USERNAME (`root`): The username to use in order to connect to MySQL.
- DB_PASSWORD (`secret`): The password to  use in order to connect to MySQL.