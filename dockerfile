FROM registry.gitlab.com/knowledge-conquest/sbt-image:1.3.8

WORKDIR /usr/src/tmp
COPY . .

# Test and compile the
RUN ~/sbt test universal:packageZipTarball

# Extract app
RUN mkdir /usr/src/app
RUN tar -zxvf target/universal/*.tgz -C ../app

WORKDIR ../app/kc-authentication-service-0.1
RUN rm -rf ../../tmp

# Add wait-for-it script
COPY wait-for-it.sh bin

EXPOSE 9000

CMD "./bin/kc-authentication-service"